﻿using K2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kol2.Controllers
{
    public class AutentifikacijaController : Controller
    {
        public ActionResult Register()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult Register(Korisnik korisnik)
        {            
             if(korisnik.KorisnickoIme.Count() <3)
            {
                ViewBag.Message = "[ERROR] Korisnicko ime mora imati minimalno 3 karaktera!";
                return View();
            }        
             if(korisnik.Sifra.Count() <8)
            {
                ViewBag.Message = "[ERROR] Sifra mora imati minimalno 8 karaktera!";
                return View();
            }


            string datum = string.Empty;

            datum += $"{korisnik.DatumRodjenja.Day}/{korisnik.DatumRodjenja.Month}/{korisnik.DatumRodjenja.Year}";
            
            
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            if (korisnici.Contains(korisnik))
            {
                ViewBag.Message = $"User with {korisnik.KorisnickoIme} already exists!";
                return View();
            }

            korisnik.Role = UserType.KUPAC;

            korisnici.Add(korisnik);
            Podaci.SacuvajKorisnika(korisnik);
            return RedirectToAction("Login", "Autentifikacija");
        }


        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string korisnickoIme, string sifra)
        {
            List<Korisnik> pretraga = (List<Korisnik>)HttpContext.Application["korisnici"];
            Korisnik kor = pretraga.Find(k => k.KorisnickoIme.Equals(korisnickoIme) && k.Sifra.Equals(sifra) && !k.Deleted);

            if (kor == null)
            {
                ViewBag.Message = $"Neispravni podaci!";
                return View("Login");
            }
            
            

            Session["Username"] = kor.KorisnickoIme;
            Session["User"] = kor;
            Session["Role"] = kor.Role.ToString();
            Session["Cart"] = new Dictionary<Proizvod, Double>();
            Session["ItemsInCart"] = 0;
            return RedirectToAction("Shop", "Proizvodi");
        }

        public ActionResult Logout()
        {
            Session["User"] = null;
            Session["Username"] = null;
            Session["Role"] = "GUEST";
            Session["Cart"] = null;

            return RedirectToAction("Index", "Home");
        }
    }
}