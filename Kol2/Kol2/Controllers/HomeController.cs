﻿using K2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kol2.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Dictionary<Proizvod, Double> cart = (Dictionary<Proizvod, Double>)Session["Cart"];
            if(cart!= null && cart.Count != 0)
            {
                return RedirectToAction("Index", "Cart");
            }

            if (Session["Username"]==null)
            {
                Session["Username"] = "Guest";
                Session["Role"] = "GUEST";
            }

            return View();
        }

        public ActionResult Users()
        {
            Dictionary<Proizvod, Double> cart = (Dictionary<Proizvod, Double>)Session["Cart"];
            if (cart != null && cart.Count != 0)
            {
                return RedirectToAction("Index", "Cart");
            }


            if (Session["Role"] != null)
            {
                if (Session["Role"].ToString() == "ADMINISTRATOR")
                {
                    List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
                    return View(korisnici);
                }
                return View("Index");
            }
            return View("Index");
        }

        public ActionResult RemoveUser(String korisnickoIme)
        {
            if (Session["Role"] != null)
            {
                if (Session["Role"].ToString() == "ADMINISTRATOR")
                {
                    try
                    {
                        List<Korisnik> ucitavanje = (List<Korisnik>)HttpContext.Application["korisnici"];
                        foreach(Korisnik k in ucitavanje)
                        {
                            if(k.KorisnickoIme==korisnickoIme)
                            {
                                k.Deleted = true;
                                ViewBag.Message = $"Korisnik {k.KorisnickoIme} uspesno izbrisan (logicki) iz baze!";
                                break;
                            }
                        }

                        Podaci.UpdateKorisnike(ucitavanje);
                                                
                        return RedirectToAction("Users","Home");
                    }
                    catch
                    {
                        return View();
                    }
                }
                return View("Index");
            }
            return View("Index");
        }


    }
}