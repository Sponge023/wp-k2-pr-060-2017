﻿using K2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kol2.Controllers
{
    public class PreviousController : Controller
    {
        // GET: Previous
        public ActionResult Index()
        {
            Dictionary<Proizvod, Double> cart = (Dictionary<Proizvod, Double>)Session["Cart"];
            if (cart == null)
                return RedirectToAction("Login", "Autentifikacija");


            if (cart != null && cart.Count != 0)
            {
                return RedirectToAction("Index", "Cart");
            }


            List<Kupovina> kupovine = Podaci.UcitajKupovine(Server.MapPath("~/App_Data/kupovine.txt"));
            List<Kupovina> korisnikoveKupovine = new List<Kupovina>();
            foreach(Kupovina k in kupovine)
            {
                if(k.Kupac.KorisnickoIme==Session["Username"].ToString())
                {
                    korisnikoveKupovine.Add(k);
                }
            }


            return View(korisnikoveKupovine);
        }

    }
}