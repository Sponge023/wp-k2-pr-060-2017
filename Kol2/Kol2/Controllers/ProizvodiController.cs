﻿using K2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kol2.Controllers
{
    public class ProizvodiController : Controller
    {
        // GET: Proizvodi
        public ActionResult Index()
        {

            return View();
        }

        public ActionResult Shop(string tip, string parametar)
        {
            Dictionary<Proizvod, Double> cart = (Dictionary<Proizvod, Double>)Session["Cart"];
            if (cart != null && cart.Count!=0)
            {
                return RedirectToAction("Index", "Cart");
            }


            List<Proizvod> proizvodi = Podaci.UcitajProizvode(Server.MapPath("~/App_Data/proizvodi.txt"));
            //(List<Proizvod>)HttpContext.Application["proizvodi"];
            if(Session["Filtrirano"]!= null)
            {
                proizvodi = (List<Proizvod>)HttpContext.Session["Filtrirano"];
                Session["Filtrirano"] = null;
            }

            switch (tip)
            {
                case "cenaA":
                    proizvodi = proizvodi.OrderBy(p => p.CenaKG).ToList();
                    break;
                case "cenaD":
                    proizvodi = proizvodi.OrderByDescending(x => x.CenaKG).ToList();
                    break;
                case "nazivA":
                    proizvodi = proizvodi.OrderBy(x => x.Naziv).ToList();
                    break;
                case "nazivD":
                    proizvodi = proizvodi.OrderByDescending(x => x.Naziv).ToList();
                    break;
                case "sortA":
                    proizvodi = proizvodi.OrderBy(x => x.Sorta).ToList();
                    break;
                case "sortD":
                    proizvodi = proizvodi.OrderByDescending(x => x.Sorta).ToList();
                    break;
                default:

                    break;
            }


            return View(proizvodi);
        }

        public ActionResult Edit(string naziv)
        {
            if (Session["Role"] != null)
            {
                if (Session["Role"].ToString() == "ADMINISTRATOR")
                {
                    List<Proizvod> proizvodi = (List<Proizvod>)HttpContext.Application["proizvodi"];

                    foreach (Proizvod p in proizvodi)
                    {
                        if (p.Naziv == naziv)
                        {
                            return View(p);
                        }

                    }
                    return View("Index");
                }

                return View("Index");
            }
            return View("Index");
        }

        [HttpPost]
        public ActionResult Izmeni(Proizvod pr,string starinaziv)
        {
            List<Proizvod> proizvodi = (List<Proizvod>)HttpContext.Application["proizvodi"];

            foreach (Proizvod p in proizvodi)
            {
                if (p.Naziv==starinaziv)
                {
                    proizvodi.Remove(p);
                    break;
                }
            }

            proizvodi.Add(pr);
            Podaci.UpdateProizvode(proizvodi);
            return RedirectToAction("Shop","Proizvodi");
        }
        public ActionResult Add()
        {


            return View();
        }
        [HttpPost]
        public ActionResult Add(Proizvod pr)
        {
            if (Session["Role"] != null)
            {
                if (Session["Role"].ToString() == "ADMINISTRATOR")
                {
                    List<Proizvod> proizvodi = (List<Proizvod>)HttpContext.Application["proizvodi"];

                    foreach (Proizvod p in proizvodi)
                    {
                        if (p.Naziv == pr.Naziv)
                        {
                            ViewBag.Message = $"[ERROR] : {pr.Naziv} vec postoji!";
                            return View();
                        }

                    }
                    ViewBag.Message =$"[INFO] Proizvod {pr.Naziv} uspesno dodat!";
                    proizvodi.Add(pr);
                    Podaci.UpdateProizvode(proizvodi);
                    return RedirectToAction("Shop","Proizvodi");
                }

                return RedirectToAction("Index","Home");
            }
            return RedirectToAction("Index","Home");

        }

        public ActionResult Remove(string naziv)
        {
            List<Proizvod> proizvodi = (List<Proizvod>)HttpContext.Application["proizvodi"];
            foreach(Proizvod pr in proizvodi)
            {
                if(pr.Naziv==naziv)
                {
                    proizvodi.Remove(pr);
                    break;

                }
            }

            Podaci.UpdateProizvode(proizvodi);
            return RedirectToAction("Shop", "Proizvodi");
            
        }

        public ActionResult Search(string vrednost, string kriterijum)
        {
            List<Proizvod> proizvodi = (List<Proizvod>)HttpContext.Application["proizvodi"];
            List<Proizvod> filtrirano = new List<Proizvod>();
            string vr = vrednost;
            string kr = kriterijum;
            switch(kriterijum)
            {
                case "naziv":
                    foreach (Proizvod pr in proizvodi)
                    {
                        if(pr.Naziv==vrednost)
                        {
                            filtrirano.Add(pr);
                        }
                    }
                    break;
                case "sorta":
                    foreach (Proizvod pr in proizvodi)
                    {
                        if (pr.Sorta == vrednost)
                        {
                            filtrirano.Add(pr);
                        }
                    }
                    break;
                case "cena":
                    foreach (Proizvod pr in proizvodi)
                    {
                        if (pr.CenaKG<=  Convert.ToDouble(vrednost))
                        {
                            filtrirano.Add(pr);
                        }
                    }
                    break;
                default:
                    break;
            }
            Session["Filtrirano"] = filtrirano;
            return RedirectToAction("Shop");
        }


    }
}