﻿using K2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kol2.Controllers
{
    public class CartController : Controller
    {

        public ActionResult Index()
        {
            Dictionary<Proizvod, Double> cart = (Dictionary<Proizvod, Double>)Session["Cart"];
            if (cart == null)
                return RedirectToAction("Login", "Autentifikacija");

            double ukupnaCena = 0;
            foreach (var proizvod in cart.Keys)
            {
                ukupnaCena += (cart[proizvod] * proizvod.CenaKG);
            }

            ViewBag.UkupnaCena = ukupnaCena;
            return View(cart);
        }

        [HttpPost]
        public ActionResult Add(double kolicina,string naziv)
        {
            Dictionary<Proizvod, Double> cart = (Dictionary<Proizvod, Double>)Session["Cart"];
            List<Proizvod> proizvodi = (List<Proizvod>)HttpContext.Application["proizvodi"];
            foreach(Proizvod pr in proizvodi)
            {
                if(pr.Naziv==naziv)
                {
                    if(pr.Raspolozivo<kolicina)
                    {
                        ViewBag.Message = $"[ERROR] Zelite uzeti vise proizvoda {naziv} nego sto je raspolozivo! Vratite se nazad i ispravite gresku!";
                        return View();
                    }
                }
            }

            Proizvod proizvod = proizvodi.Find(prod => prod.Naziv.Equals(naziv));
            if (cart.ContainsKey(proizvod))
            {
                cart[proizvod] = cart[proizvod] + kolicina;
                
            }
            else
            {
                cart.Add(proizvod, kolicina);
            }


            Kupovina k = new Kupovina((Korisnik)Session["user"], proizvod, DateTime.Now, kolicina, (proizvod.CenaKG * kolicina));
            Podaci.SacuvajKupovinu(k);

            Session["ItemsInCart"] = "1";
            ViewBag.Message = $"Proizvod [{naziv}] successfully added to the Cart!";
            return RedirectToAction("Index", "Cart");
        }

        public ActionResult Buy()
        {
            Dictionary<Proizvod, Double> cart = (Dictionary<Proizvod, Double>)Session["Cart"];
            if (cart == null)
                return RedirectToAction("Login", "Autentifikacija");

            Proizvod temp = new Proizvod();
            List<Proizvod> proizvodi = (List<Proizvod>)HttpContext.Application["proizvodi"];
            //Skidanje sa stanja
            foreach (Proizvod pr in cart.Keys)
            {
                foreach(Proizvod pr2 in proizvodi)
                {
                    if(pr2.Naziv==pr.Naziv)
                    {
                        pr2.Raspolozivo -= cart[pr];
                        temp = pr;
                        break;
                    }
                }
            }
            Podaci.UpdateProizvode(proizvodi);

            Session["Cart"] = new Dictionary<Proizvod, Double>();

            ViewBag.Message = "Cart successfully emptied! Proceed to the checkpoint!";

            Session["ItemsInCart"] = 0;
            return RedirectToAction("Shop", "Proizvodi");
        }

        [HttpPost]
        public ActionResult Details(string naziv)
        {
            if(Session["ItemsInCart"]!= null && Session["ItemsInCart"].ToString() == "1")
            {
                RedirectToAction("Shop", "Proizvodi");
            }

            List<Proizvod> proizvodi = (List<Proizvod>)HttpContext.Application["proizvodi"];
            foreach(Proizvod pr in proizvodi)
            {
                if (pr.Naziv == naziv)
                {
                    return View(pr);
                }
            }


            return View();
        }

    }
}