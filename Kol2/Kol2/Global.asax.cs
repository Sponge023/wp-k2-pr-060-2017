﻿using K2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Kol2
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            

            List<Korisnik> korisnici = Podaci.UcitajKorisnike(Server.MapPath("~/App_Data/korisnici.txt"));
            HttpContext.Current.Application["korisnici"] = korisnici;

            List<Proizvod> proizvodi = Podaci.UcitajProizvode(Server.MapPath("~/App_Data/proizvodi.txt"));
            HttpContext.Current.Application["proizvodi"] = proizvodi;

            List<Kupovina> kupovine = Podaci.UcitajKupovine(Server.MapPath("~/App_Data/kupovine.txt"));
            HttpContext.Current.Application["kupovine"] = kupovine;

        }
    }
}
