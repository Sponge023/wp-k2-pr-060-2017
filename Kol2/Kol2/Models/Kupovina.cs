﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace K2.Models
{
    public class Kupovina
    {
        public Korisnik Kupac { get; set; }
        public Proizvod Proizvod { get; set; }
        public DateTime DatumKupovine { get; set; }
        public double Kolicina { get; set; }
        public double Naplaceno { get; set; }


        public Kupovina()
        {
        }

        public Kupovina(Korisnik kupac, Proizvod proizvod, DateTime datumKupovine, double kolicina, double naplaceno)
        {
            Kupac = kupac;
            Proizvod = proizvod;
            DatumKupovine = datumKupovine;
            Kolicina = kolicina;
            Naplaceno = naplaceno;
        }
    }
}