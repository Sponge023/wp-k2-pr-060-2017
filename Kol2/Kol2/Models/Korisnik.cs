﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace K2.Models
{
    public class Korisnik
    {
        public string KorisnickoIme { get; set; }
        public string Sifra { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Email { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public UserType Role { get; set; }
        public bool Deleted { get; set; }


        public Korisnik()
        {
        }

        public Korisnik(string korisnickoIme, string sifra, string ime, string prezime, string email, DateTime datumRodjenja, UserType role,bool deleted)
        {
            KorisnickoIme = korisnickoIme;
            Sifra = sifra;
            Ime = ime;
            Prezime = prezime;
            Email = email;
            DatumRodjenja = datumRodjenja;
            Role = role;
            Deleted = deleted;
        }
    }
}