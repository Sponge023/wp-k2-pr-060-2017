﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace K2.Models
{
    public class Podaci
    {

        public static List<Korisnik> UcitajKorisnike(string putanja)
        {
            List<Korisnik> korisnici = new List<Korisnik>();
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string red = "";
            while ((red = sr.ReadLine()) != null)
            {
                string[] indeks = red.Split('|');
                Korisnik k = new Korisnik(indeks[0],indeks[1],indeks[2],indeks[3],indeks[4],Convert.ToDateTime(indeks[5]), (UserType)Enum.Parse(typeof(UserType),indeks[6]), Convert.ToBoolean(indeks[7]));
                korisnici.Add(k);
            }
                sr.Close();
                stream.Close();

                return korisnici;
        }

        internal static List<Kupovina> UcitajKupovine(string putanja)
        {
            List<Kupovina> kupovine = new List<Kupovina>();
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string red = "";
            while ((red = sr.ReadLine()) != null)
            {
                string[] indeks = red.Split('|');


                Korisnik k = new Korisnik();
                k.KorisnickoIme = indeks[0];
                Proizvod p = new Proizvod();
                p.Naziv = indeks[1];
                Kupovina ku = new Kupovina(k, p, Convert.ToDateTime(indeks[2]), Convert.ToDouble(indeks[3]), Convert.ToDouble(indeks[4]));
                kupovine.Add(ku);
            }
            sr.Close();
            stream.Close();

            return kupovine;
        }

        public static List<Proizvod> UcitajProizvode(string putanja)
        {
            List<Proizvod> proizvodi = new List<Proizvod>();
            FileStream stream = new FileStream(putanja, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string red = "";
            while ((red = sr.ReadLine()) != null)
            {
                string[] indeks = red.Split('|');
                Proizvod p = new Proizvod(indeks[0],indeks[1],indeks[2], indeks[3],indeks[4],indeks[5],Double.Parse(indeks[6]),Double.Parse(indeks[7]));
                proizvodi.Add(p);
            }
            sr.Close();
            stream.Close();

            return proizvodi;
        }

        public static void SacuvajKorisnika(Korisnik korisnik)
        {
            string putanja = HostingEnvironment.MapPath("~/App_Data/korisnici.txt");
            FileStream stream = new FileStream(putanja, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            sw.WriteLine($"{korisnik.KorisnickoIme}|{korisnik.Sifra}|{korisnik.Ime}|{korisnik.Prezime}|{korisnik.Email}|{korisnik.DatumRodjenja.ToString("dd'/'MM'/'yyyy")}|{korisnik.Role.ToString()}|{korisnik.Deleted}");
            
            sw.Close();
            stream.Close();

            return;
        }
        public static void SacuvajKupovinu(Kupovina kupovina)
        {
            string putanja = HostingEnvironment.MapPath("~/App_Data/kupovine.txt");
            FileStream stream = new FileStream(putanja, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            sw.WriteLine($"{kupovina.Kupac.KorisnickoIme}|{kupovina.Proizvod.Naziv}|{kupovina.DatumKupovine.ToString("dd'/'MM'/'yyyy")}|{kupovina.Kolicina}|{kupovina.Naplaceno}");
            
            sw.Close();
            stream.Close();

            return;
        }

        public static void SacuvajProizvod(Proizvod proizvod)
        {
            string putanja = HostingEnvironment.MapPath("~/App_Data/proizvodi.txt");
            FileStream stream = new FileStream(putanja, FileMode.Append);
            StreamWriter sw = new StreamWriter(stream);

            sw.WriteLine($"{proizvod.Naziv}|{proizvod.Sorta}|{proizvod.PopularnoIme}|{proizvod.Boja}|{proizvod.Poreklo}|{proizvod.Opis}|{proizvod.CenaKG}|{proizvod.Raspolozivo}");

            sw.Close();
            stream.Close();

            return;
        }

        public static void UpdateKorisnike(List<Korisnik> korisnici)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/korisnici.txt");
            FileStream stream = new FileStream(path, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);

            foreach (Korisnik korisnik in korisnici)
            {
                sw.WriteLine($"{korisnik.KorisnickoIme}|{korisnik.Sifra}|{korisnik.Ime}|{korisnik.Prezime}|{korisnik.Email}|{korisnik.DatumRodjenja.ToString("dd'/'MM'/'yyyy")}|{korisnik.Role.ToString()}|{korisnik.Deleted}");

            }

            sw.Close();
            stream.Close();
        }

        public static void UpdateProizvode(List<Proizvod> proizvodi)
        {
            string path = HostingEnvironment.MapPath("~/App_Data/proizvodi.txt");
            FileStream stream = new FileStream(path, FileMode.Create);
            StreamWriter sw = new StreamWriter(stream);

            foreach (Proizvod proizvod in proizvodi)
            {
                sw.WriteLine($"{proizvod.Naziv}|{proizvod.Sorta}|{proizvod.PopularnoIme}|{proizvod.Boja}|{proizvod.Poreklo}|{proizvod.Opis}|{proizvod.CenaKG}|{proizvod.Raspolozivo}");

            }

            sw.Close();
            stream.Close();
        }

        
    }
}