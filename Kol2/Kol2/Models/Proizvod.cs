﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace K2.Models
{
    public class Proizvod
    {
        public string Naziv { get; set; }
        public string Sorta { get; set; }
        public string PopularnoIme { get; set; }
        public string Boja { get; set; }
        public string Poreklo { get; set; }
        public string Opis { get; set; }
        public double CenaKG { get; set; }
        public double Raspolozivo { get; set; }

        public Proizvod()
        {
        }

        public Proizvod(string naziv, string sorta, string popularnoIme, string boja, string poreklo, string opis, double cenaKG, double raspolozivo)
        {
            Naziv = naziv;
            Sorta = sorta;
            PopularnoIme = popularnoIme;
            Boja = boja;
            Poreklo = poreklo;
            Opis = opis;
            CenaKG = cenaKG;
            Raspolozivo = raspolozivo;
        }
    }
}